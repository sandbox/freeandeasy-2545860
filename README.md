INTRODUCTION
------------
This module allows admins to create twig filter in PHP directly in the
administration interface.

Warning: Only give access to this module to trustworthy users, since user
entered PHP gets executed.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/sandbox/freeandeasy/2545860

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/2545860


REQUIREMENTS
------------
No special requirements


INSTALLATION
------------
 * Install as you would normally install a contributed Drupal module. See:
   https://www.drupal.org/documentation/install/modules-themes/modules-8
   for further information.


CONFIGURATION
-------------
 * Step 1: Create Filter
   go to /admin/config/system/twigfilter
   create a new filter by clicking the "Add Twigfilter" button
   name your filter and enter your filter code, save
 * Step 2: Use Filter
   use your filter anywhere (template/views etc) like this:
   {{ 'mytext'|twigfilter('my-filter-machinename') }}


MAINTAINERS
-----------
Current maintainers:
 * Cornelius Kahns (FreeAndEasy) - https://www.drupal.org/u/freeandeasy
