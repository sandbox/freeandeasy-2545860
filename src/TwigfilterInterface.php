<?php
/**
 * @file
 * Contains \Drupal\twigfilter\TwigfilterInterface.
 */

namespace Drupal\twigfilter;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining a Twigfilter entity.
 */
interface TwigfilterInterface extends ConfigEntityInterface {

  /**
   * Return the filtercode.
   *
   * @return string
   *   filtercode
   */
  public function filtercode();

}
