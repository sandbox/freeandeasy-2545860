<?php
/**
 * @file
 * Contains \Drupal\custom_twig_filters\TwigExtension\TestExtension.
 */

namespace Drupal\twigfilter\TwigExtension;

use Drupal\Core\Template\TwigExtension;
use Drupal\twigfilter\Entity\Twigfilter;

use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Routing\UrlGeneratorInterface;
use Drupal\Core\Theme\ThemeManagerInterface;

/**
 * A test Twig extension that adds a custom function and a custom filter.
 */
class TwigfilterExtension extends TwigExtension {

  /**
   * Generates a list of all Twig filters that this extension defines.
   *
   * @return array
   *   A key/value array that defines custom Twig filters.
   */
  public function getFilters() {
    return array(
      new \Twig_SimpleFilter('twigfilter', [$this, 'customFilters'], ['needs_environment' => TRUE, 'is_safe' => ['html']]),
    );
  }

  /**
   * Gets a unique identifier for this Twig extension.
   *
   * @return string
   *   A unique identifier for this Twig extension.
   */
  public function getName() {
    return 'twigfilter.twigfilter_extension';
  }


  /**
   * Runs the custom twig filters.
   *
   * @param \Twig_Environment $env
   *   A Twig_Environment instance.
   * @param mixed[]|\Traversable|null $string
   *   The pieces to join.
   * @param string $$filtername
   *   The delimiter with which to join the string. Defaults to an empty string.
   *   This value is expected to be safe for output and user provided data
   *   should never be used as a glue.
   *
   * @return string
   *   The strings joined together.
   */
  public static function customFilters(\Twig_Environment $env, $string, $filtername = '') {

    // Get array of twigfilters.
    $query = \Drupal::entityQuery('twigfilter');
    $twigfilters = $query->execute();

    if (!$filtername) {
      drupal_set_message(t('no twig filtername set'), 'error');
      return $string;
    }

    if (isset($twigfilters[$filtername])) {
      // Load filter and apply to string.
      $twigfilter_entity = Twigfilter::load($filtername);
      $filter_function = create_function('$str', $twigfilter_entity->filtercode());

      return $filter_function($string);
    }
    else {
      drupal_set_message(t('missing twig filter: @filtername', array('@filtername' => $filtername)), 'error');
      return $string;
    }
  }

}
