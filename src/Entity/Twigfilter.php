<?php
/**
 * @file
 * Contains \Drupal\twigfilter\Entity\Twigfilter.
 */

namespace Drupal\twigfilter\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\twigfilter\TwigfilterInterface;

/**
 * Defines the Twigfilter entity.
 *
 * @ConfigEntityType(
 *   id = "twigfilter",
 *   label = @Translation("Twigfilter"),
 *   handlers = {
 *     "list_builder" = "Drupal\twigfilter\Controller\TwigfilterListBuilder",
 *     "form" = {
 *       "add" = "Drupal\twigfilter\Form\TwigfilterForm",
 *       "edit" = "Drupal\twigfilter\Form\TwigfilterForm",
 *       "delete" = "Drupal\twigfilter\Form\TwigfilterDeleteForm"
 *     }
 *   },
 *   config_prefix = "twigfilter",
 *   admin_permission = "administer twigfilters",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *   },
 *   links = {
 *     "edit-form" = "/admin/config/system/twigfilter/{twigfilter}",
 *     "delete-form" = "/admin/config/system/twigfilter/{twigfilter}/delete"
 *   }
 * )
 */
class Twigfilter extends ConfigEntityBase implements TwigfilterInterface {

  /**
   * The Twigfilter ID.
   *
   * @var string
   */
  public $id;

  /**
   * The Twigfilter label.
   *
   * @var string
   */
  public $label;

  /**
   * The Twigfilter ID.
   *
   * @var string
   */
  public $filtercode;

  /**
   * Return the filtercode.
   *
   * @return string
   *   filtercode
   */
  public function filtercode() {
    return $this->filtercode;
  }

}
