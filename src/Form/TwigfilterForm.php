<?php
/**
 * @file
 * Contains \Drupal\twigfilter\Form\TwigfilterForm.
 */

namespace Drupal\twigfilter\Form;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityForm;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\Query\QueryFactory;
use Drupal\Core\Form\FormStateInterface;


/**
 * Builds the form to add/edit a Twigfilter.
 */
class TwigfilterForm extends EntityForm {

  /**
   * Prepares the form.
   *
   * @param \Drupal\Core\Entity\Query\QueryFactory $entity_query
   *   The entity query.
   */
  public function __construct(QueryFactory $entity_query) {
    $this->entityQuery = $entity_query;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.query')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $twigfilter = $this->entity;

    $form['label'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $twigfilter->label(),
      '#description' => $this->t("Label for the Twigfilter."),
      '#required' => TRUE,
    );
    $form['id'] = array(
      '#type' => 'machine_name',
      '#default_value' => $twigfilter->id(),
      '#machine_name' => array(
        'exists' => array($this, 'exist'),
      ),
      '#disabled' => !$twigfilter->isNew(),
    );

    $warnings = '<p><strong>' . $this->t('If you are unfamiliar with PHP do not use this module.') . '</strong> ' . $this->t('Experimenting with PHP may corrupt your database, render your site inoperable, or significantly compromise security.') . '</p>';
    $warnings .= '<p>' . $this->t('Notes:') . '</p>';
    $warnings .= '<ul><li>' . $this->t('Remember to double-check each line for syntax and logic errors <strong>before</strong> saving.') . '</li>';
    $warnings .= '<li>' . $this->t('Statements must be correctly terminated with semicolons.') . '</li>';
    $warnings .= '<li>' . $this->t('Develop and test your PHP code using a separate test script before deploying on a production site.') . '</li></ul><p>&nbsp;</p>';

    $form['warnings'] = array(
      '#type' => 'item',
      '#markup' => $warnings,
    );

    $form['filtercode'] = array(
      '#type' => 'textarea',
      '#title' => $this->t('Filtercode'),
      '#default_value' => $twigfilter->filtercode(),
      '#description' => $this->t('Filtercode for the Twigfilter. The input variable is $str, return the desired filter ouput.'),
      '#required' => TRUE,
      '#rows' => 20,
      '#placeholder' => $this->t('Your PHP code.'),
      '#prefix' => '<code><?php function filter_function($str) {</code>',
      '#suffix' => '<code>} ?></code>',
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $twigfilter = $this->entity;
    $status = $twigfilter->save();

    if ($status) {
      drupal_set_message($this->t('Saved the %label Twigfilter.', array(
        '%label' => $twigfilter->label(),
      )));
    }
    else {
      drupal_set_message($this->t('The %label Twigfilter was not saved.', array(
        '%label' => $twigfilter->label(),
      )));
    }

    $form_state->setRedirect('twigfilter.list');
  }

  /**
   * Checks if the filter exists.
   */
  public function exist($id) {
    $entity = $this->entityQuery->get('twigfilter')
      ->condition('id', $id)
      ->execute();
    return (bool) $entity;
  }

}
